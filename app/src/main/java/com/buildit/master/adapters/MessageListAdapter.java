package com.buildit.master.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.buildit.master.R;
import com.buildit.master.models.Messages;

import java.util.List;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {

    List<Messages> messages;

    private String sLastMessageBody;
    private String sMessageUser;
    private String sLastMessageTime;

    Context context;

    public MessageListAdapter(List<Messages> messages, Context context) {
        this.messages = messages;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView lastMessageBody;
        private TextView messageClient;
        private TextView lastMessageTime;

        public ViewHolder(final View view){
            super(view);

            lastMessageBody = (TextView)view.findViewById(R.id.message_item_body);
            messageClient = (TextView)view.findViewById(R.id.message_item_client);
            lastMessageTime = (TextView)view.findViewById(R.id.message_item_time);
        }
    }

    @Override
    public MessageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new MessageListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MessageListAdapter.ViewHolder holder, int position) {
        final Messages message = messages.get(position);

        sMessageUser = message.getUserTwo();

        holder.messageClient.setText(sMessageUser);
        holder.lastMessageTime.setText("10:20");

    }

    @Override
    public int getItemCount() {
        return 1;
    }
}
