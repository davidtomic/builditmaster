package com.buildit.master.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.buildit.master.R;
import com.buildit.master.models.Job;
import com.buildit.master.requests.Request;

import java.util.List;

/**
 * Created by tomicdavid on 11/2/17.
 */

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.ViewHolder> {

    private List<Job> jobList;
    private Context context;

    private String sJobTitle;
    private String sJobOwner;
    private int sJobId;
    private Image sJobOwnerAvatar;
    private boolean saved;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView jobID;
        private TextView jobOwner;
        private TextView jobTitle;
        private ImageView jobOwnerAvatar;
        private ImageButton jobSaved;

        private Request.SaveMasterJob saveJob = new Request.SaveMasterJob();
        private Request.UnsaveMasterJob unsaveJob = new Request.UnsaveMasterJob();

        public ViewHolder(final View itemView) {
            super(itemView);

            jobID = itemView.findViewById(R.id.job_id);
            jobOwner = itemView.findViewById(R.id.job_owner);
            jobTitle = itemView.findViewById(R.id.job_title);
            jobOwnerAvatar = itemView.findViewById(R.id.job_avatar);
            jobSaved = itemView.findViewById(R.id.job_saved);
            jobSaved.setTag("1");

            jobSaved.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(jobSaved.getTag()=="1"){
                        jobSaved.setImageResource(android.R.drawable.btn_star_big_on);
                        jobSaved.setTag("2");
                        saveJob.execute(String.valueOf(sJobId), sJobOwner);
                        Toast.makeText(context, "Job saved", Toast.LENGTH_SHORT).show();
                    } else if (jobSaved.getTag()=="2"){
                        jobSaved.setImageResource(android.R.drawable.btn_star_big_off);
                        jobSaved.setTag("1");
                        unsaveJob.execute(String.valueOf(sJobId), sJobOwner);
                        Toast.makeText(context, "Job unsaved", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    public JobListAdapter(List<Job> list, Context context) {
        this.jobList = list;
        this.context = context;
    }

    @Override
    public JobListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_job, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(JobListAdapter.ViewHolder holder, int position) {
        final Job job = jobList.get(position);

        sJobTitle = job.getJobTitle();
        sJobOwner = job.getJobOwner();
        saved = job.isSaved();
        sJobId = job.getJobId();


        holder.jobTitle.setText(sJobTitle);
        holder.jobOwner.setText(sJobOwner);

        if(saved){
            holder.jobSaved.setImageResource(android.R.drawable.star_big_on);
        }

    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }
}
