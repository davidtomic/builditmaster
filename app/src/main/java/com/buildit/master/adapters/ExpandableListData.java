package com.buildit.master.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tomicdavid on 11/10/17.
 */

public class ExpandableListData {

    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> jobTypes = new ArrayList<String>();
        jobTypes.add("Vodoinstalater");
        jobTypes.add("Elektroinstalater");
        jobTypes.add("Plinoinstalater");
        jobTypes.add("Limar");
        jobTypes.add("Parketar");
        jobTypes.add("Zidar");
        jobTypes.add("Armirac");
        jobTypes.add("Keramicar");
        jobTypes.add("Tesar");

        return expandableListDetail;
    }

}
