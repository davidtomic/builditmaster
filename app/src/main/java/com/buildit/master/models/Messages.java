package com.buildit.master.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 11/10/17.
 */

public class Messages {

    @SerializedName("message_id")
    private int messageId;
    @SerializedName("user_one")
    private String userOne;
    @SerializedName("user_two")
    private String userTwo;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getUserOne() {
        return userOne;
    }

    public void setUserOne(String userOne) {
        this.userOne = userOne;
    }

    public String getUserTwo() {
        return userTwo;
    }

    public void setUserTwo(String userTwo) {
        this.userTwo = userTwo;
    }
}
