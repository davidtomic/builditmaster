package com.buildit.master.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 11/2/17.
 */

public class Job {

    @SerializedName("job_id")
    private int job_id;
    @SerializedName("job_owner")
    private String job_owner;
    @SerializedName("job_location")
    private String job_owner_location;
    @SerializedName("job_phone_number")
    private String job_owner_number;
    @SerializedName("job_title")
    private String job_title;
    @SerializedName("job_description")
    private String job_description;
    @SerializedName("advertised")
    private boolean advertised;

    private boolean saved;

    public int getJobId() {
        return job_id;
    }

    public void setJobId(int job_id) {
        this.job_id = job_id;
    }

    public String getJobOwner() {
        return job_owner;
    }

    public void setJobOwner(String job_owner) {
        this.job_owner = job_owner;
    }

    public String getJobOwnerLocation() {
        return job_owner_location;
    }

    public void setJobOwnerLocation(String job_owner_location) {
        this.job_owner_location = job_owner_location;
    }

    public String getJobOwnerNumber() {
        return job_owner_number;
    }

    public void setJobOwnerNumber(String job_owner_number) {
        this.job_owner_number = job_owner_number;
    }

    public String getJobTitle() {
        return job_title;
    }

    public void setJobTitle(String job_title) {
        this.job_title = job_title;
    }

    public String getJobDescription() {
        return job_description;
    }

    public void setJobDescription(String job_description) {
        this.job_description = job_description;
    }

    public boolean getAdvertised() {
        return advertised;
    }

    public void setAdvertised(boolean advertised) {
        this.advertised = advertised;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }
}
