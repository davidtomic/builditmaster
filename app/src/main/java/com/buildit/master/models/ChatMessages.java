package com.buildit.master.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 11/2/17.
 */

public class ChatMessages {

    @SerializedName("message_id")
    private int messageId;
    @SerializedName("message_sender")
    private String messageSender;
    @SerializedName("message_reciever")
    private String messageReciever;
    @SerializedName("message_body")
    private String messageBody;
    @SerializedName("date_send")
    private String dateSend;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageReciever() {
        return messageReciever;
    }

    public void setMessageReciever(String messageReciever) {
        this.messageReciever = messageReciever;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getDateSend() {
        return dateSend;
    }

    public void setDateSend(String dateSend) {
        this.dateSend = dateSend;
    }
}
