package com.buildit.master.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 11/10/17.
 */

public class AboutMaster {

    @SerializedName("about_master")
    private String aboutMaster;

    public String getAboutMaster() {
        return aboutMaster;
    }

    public void setAboutMaster(String aboutMaster) {
        this.aboutMaster = aboutMaster;
    }
}
