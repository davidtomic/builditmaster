package com.buildit.master.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class SavedJobs {

    @SerializedName("job_id")
    private int jobId;
    @SerializedName("master_email")
    private String masterEmail;

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getMasterEmail() {
        return masterEmail;
    }

    public void setMasterEmail(String masterEmail) {
        this.masterEmail = masterEmail;
    }
}
