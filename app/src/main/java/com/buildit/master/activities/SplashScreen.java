package com.buildit.master.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.buildit.master.R;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class SplashScreen extends Activity {

    private SharedPreferences sharedPreferences;

    private String userEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        userEmail = sharedPreferences.getString("user_email", "fail");

        if(userEmail.equals("fail")||userEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        userEmail = sharedPreferences.getString("user_email", "fail");
        if(userEmail.equals("fail")||userEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        userEmail = sharedPreferences.getString("user_email", "fail");
        if(userEmail.equals("fail")||userEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        userEmail = sharedPreferences.getString("user_email", "fail");
        if(userEmail.equals("fail")||userEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
        }
    }
}

