package com.buildit.master.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.buildit.master.R;
import com.buildit.master.fragments.Login;
import com.buildit.master.fragments.Register;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class LoginActivity extends FragmentActivity {

    private TextView loginRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setLoginFragment();

        loginRegister = findViewById(R.id.login_to_register_button);

        loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginRegister.getText().toString().equals(getString(R.string.login_to_register))){
                    setRegisterFragment();
                    loginRegister.setText(getString(R.string.register_to_login));
                } else if (loginRegister.getText().toString().equals(getString(R.string.register_to_login))) {
                    setLoginFragment();
                    loginRegister.setText(getString(R.string.login_to_register));
                }
            }
        });

    }

    public void setLoginFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Login fragment = new Login();
        fragmentTransaction.replace(R.id.login_frame, fragment);
        fragmentTransaction.commit();
    }

    public void setRegisterFragment(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Register fragment = new Register();
        fragmentTransaction.replace(R.id.login_frame, fragment);
        fragmentTransaction.commit();
    }
}
