package com.buildit.master.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.buildit.master.R;
import com.buildit.master.adapters.ViewPagerAdapter;
import com.buildit.master.fragments.AdvertisedJobs;
import com.buildit.master.fragments.AllJobs;
import com.buildit.master.fragments.SavedJobs;
import com.buildit.master.models.User;
import com.buildit.master.requests.Request;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

public class JobsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;

    TextView navigation_user_name;
    TextView navigation_user_email;
    Button logout;

    String currentUserEmail = "";
    User currentUser;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);

        checkIfUserLoggedIn();

        getCurrentUser();

        setView();
    }

    public void getCurrentUser(){

        Request.GetMasterCredentials getMasterCredentials = new Request.GetMasterCredentials();
        String sCurrentUser = "";

        try {
             sCurrentUser = getMasterCredentials.execute(currentUserEmail).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        currentUser = convertJsonToList(sCurrentUser);
    }

    public void checkIfUserLoggedIn(){
        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        currentUserEmail = sharedPreferences.getString("user_email", "");

        if(currentUserEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void setView(){

        setViewPager();

        setTabLayout();

        setToolbar();

        setDrawer();

        setNavigationView();

        navigation_user_name = navigationView.getHeaderView(0).findViewById(R.id.navigation_user_name);
        navigation_user_email = navigationView.getHeaderView(0).findViewById(R.id.navigation_user_email);

        logout = navigationView.getHeaderView(0).findViewById(R.id.logout);

        navigation_user_name.setText(currentUser.getName());
        navigation_user_email.setText(currentUser.getEmail());

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("user_email", "");
                editor.putString("user_password", "");
                editor.apply();
                editor.commit();
                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    public void setViewPager(){
        viewPager = (ViewPager)findViewById(R.id.job_viewpager);
        setupViewPager(viewPager);
    }

    public void setTabLayout(){
        tabLayout = (TabLayout)findViewById(R.id.job_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void setNavigationView(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putString("currentUserEmail", currentUserEmail);

        SavedJobs savedJobs = new SavedJobs();
        savedJobs.setArguments(bundle);

        AdvertisedJobs advertisedJobs = new AdvertisedJobs();

        AllJobs allJobs = new AllJobs();

        adapter.addFragment(allJobs, "All jobs");
        adapter.addFragment(savedJobs, "Saved jobs");
        adapter.addFragment(advertisedJobs, "Advertised");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_jobs) {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
            finish();
            return true;
        } if (id == R.id.nav_messages) {
            Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static User convertJsonToList(String jsonString){
        if(!isValid(jsonString)){
            return null;
        }
        return new Gson().fromJson(jsonString, User.class);
    }

    public static boolean isValid(String json){
        try{
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

}
