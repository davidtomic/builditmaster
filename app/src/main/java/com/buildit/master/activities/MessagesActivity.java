package com.buildit.master.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.buildit.master.R;
import com.buildit.master.fragments.Message;
import com.buildit.master.models.User;
import com.buildit.master.requests.Request;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class MessagesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;

    TextView navigation_user_name;
    TextView navigation_user_email;

    String currentUserEmail = "";
    User currentUser;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        checkIfUserLoggedIn();

        getCurrentUser();

        setView();
    }

    public void setView(){

        setToolbar();

        setDrawer();

        setNavigationView();

        navigation_user_name = navigationView.getHeaderView(0).findViewById(R.id.navigation_user_name);
        navigation_user_email = navigationView.getHeaderView(0).findViewById(R.id.navigation_user_email);

        navigation_user_name.setText(currentUser.getName());
        navigation_user_email.setText(currentUser.getEmail());

        setFragment();
    }

    public void setFragment(){

        Bundle bundle = new Bundle();
        bundle.putString("currentUserEmail", currentUserEmail);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Message fragment = new Message();
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.message_fragment, fragment);
        fragmentTransaction.commit();

    }

    public void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void setNavigationView(){
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void getCurrentUser(){

        Request.GetMasterCredentials getMasterCredentials = new Request.GetMasterCredentials();
        String sCurrentUser = "";

        try {
            sCurrentUser = getMasterCredentials.execute(currentUserEmail).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        currentUser = convertJsonToList(sCurrentUser);
    }

    public void checkIfUserLoggedIn(){
        sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        currentUserEmail = sharedPreferences.getString("user_email", "");

        if(currentUserEmail.equals("")){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public static User convertJsonToList(String jsonString){
        if(!isValid(jsonString)){
            return null;
        }
        return new Gson().fromJson(jsonString, User.class);
    }

    public static boolean isValid(String json){
        try{
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_jobs) {
            Intent intent = new Intent(getApplicationContext(), JobsActivity.class);
            startActivity(intent);
            finish();
            return true;
        } if (id == R.id.nav_messages) {
            Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
            startActivity(intent);
            finish();
            return true;
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
