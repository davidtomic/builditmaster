package com.buildit.master.requests;

import android.os.AsyncTask;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class Request {

    public static class GetAdvertisedJobs extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            String jobs = "";

            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/jobs/get_advertised_jobs.php");

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoOutput(true);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                //do something with response
                is = conn.getInputStream();

                jobs = convertStreamToString(is);

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return jobs;
        }

    }

    public static class GetAllJobs extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            String jobs = "";

            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/jobs/get_jobs.php");

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoOutput(true);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                //do something with response
                is = conn.getInputStream();

                jobs = convertStreamToString(is);

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return jobs;
        }

    }

    public static class GetSavedJobs extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String jobs = "";

            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/jobs/get_master_jobs.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoOutput(true);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                os.flush();

                //do something with response
                is = conn.getInputStream();

                jobs = convertStreamToString(is);

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return jobs;
        }

    }

    public static class SaveMasterJob extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            OutputStream os = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/jobs/set_job_saved.php");

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                jsonObject.put("job_id", params[1]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                os.flush();

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

    }

    public static class UnsaveMasterJob extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            OutputStream os = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/jobs/set_job_unsaved.php");

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                jsonObject.put("job_id", params[1]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                os.flush();

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

    }

    public static class AddOfferRequest extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            OutputStream os = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/job_offers/add_job_offer.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("hand_offer", params[0]);
                jsonObject.put("material_offer", params[1]);
                jsonObject.put("user_email", params[2]);
                jsonObject.put("master_email", params[3]);
                jsonObject.put("additional_info", params[4]);
                jsonObject.put("job_title", params[5]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                //send data
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                //clean up
                os.flush();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public static class GetMasterCredentials extends AsyncTask<String, Void, String> {

        String credentials = "";

        @Override
        protected String doInBackground(String... params) {
            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/masters/get_master_by_email.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                //send data
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                //clean up
                os.flush();

                //do something with response
                is = conn.getInputStream();

                credentials = convertStreamToString(is);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            String tCredentials = credentials.trim();
            if (tCredentials.equals("false")) {
                return "";
            } else {
                return credentials;
            }

        }
    }

    public static class AddAboutMaster extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            OutputStream os = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/masters/add_about_master.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                jsonObject.put("about", params[1]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                //send data
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                //clean up
                os.flush();

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public static class GetAboutMaster extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String about = "";

            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/masters/get_about_master.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoOutput(true);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                os.flush();

                //do something with response
                is = conn.getInputStream();

                about = convertStreamToString(is);

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return about.trim();
        }

    }

    public static class LoginRequests extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(final String... params) {
            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            Boolean valid = false;
            try {
                URL url = new URL("http://192.168.1.46/fixit/users/user_valid_login.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                jsonObject.put("password", params[1]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                String auth = params[0] + ":" + params[1];

                byte[] data = auth.getBytes("UTF-8");
                String base64 = Base64.encodeToString(data, Base64.DEFAULT);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");
                conn.addRequestProperty("Authorization", "Basic " + base64);

                //open
                conn.connect();

                //send data
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                //clean up
                os.flush();

                //do something with response
                is = conn.getInputStream();

                String sValid = convertStreamToString(is);

                sValid = sValid.trim();

                if (sValid.equals("1")) {
                    valid = true;
                } else {
                    valid = false;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            if (valid) {
                return true;
            } else {
                return false;
            }

        }
    }

    public static class RegisterRequest extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {

            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/users/add_user.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", params[0]);
                jsonObject.put("email", params[1]);
                jsonObject.put("password", params[2]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();
                //send data
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());
                //clean up
                os.flush();
                //do something with response
                is = conn.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return null;

        }
    }

    public static class GetMasterMessages extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String messages = "";

            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.1.46/fixit/messages/get_message.php");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", params[0]);
                String message = jsonObject.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoOutput(true);

                //make some HTTP header nicety
                conn.setRequestProperty("Content-Type", "application/json");

                //open
                conn.connect();

                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());

                os.flush();

                //do something with response
                is = conn.getInputStream();

                messages = convertStreamToString(is);

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                    is.close();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            return messages;
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
