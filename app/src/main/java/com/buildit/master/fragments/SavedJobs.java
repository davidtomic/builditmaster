package com.buildit.master.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.buildit.master.R;
import com.buildit.master.adapters.JobListAdapter;
import com.buildit.master.models.Job;
import com.buildit.master.requests.Request;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class SavedJobs extends Fragment {

    RecyclerView mRecyclerView;

    Context context;

    String currentUserEmail;

    List<com.buildit.master.models.SavedJobs> savedJobses = new ArrayList<>();

    List<Job> jobs = new ArrayList<>();
    List<Job> savedJobs = new ArrayList<>();

    private JobListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jobs_list, container, false);

        context = getActivity();

        currentUserEmail = getArguments().getString("currentUserEmail");

        mRecyclerView = view.findViewById(R.id.jobs_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        getAllJobs();

        getSavedJobsIds();

        setSavedJobs();

        for (Job job:jobs){
            if(job.isSaved()){
                savedJobs.add(job);
            }
        }

        adapter = new JobListAdapter(savedJobs, context);
        mRecyclerView.setAdapter(adapter);

        return view;

    }

    public void getAllJobs(){
        Request.GetAllJobs getAllJobs = new Request.GetAllJobs();

        String jsonString = null;

        try {
            jsonString = getAllJobs.execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Type listType = new TypeToken<List<Job>>() {
        }.getType();

        jobs = getPostListFromJson(jsonString, listType);
    }

    public void getSavedJobsIds(){
        Request.GetSavedJobs getSavedJobs = new Request.GetSavedJobs();

        String sSavedJobs = "";

        try {
            sSavedJobs = getSavedJobs.execute(currentUserEmail).get();
            sSavedJobs.trim();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if(!sSavedJobs.isEmpty()) {
            Type type = new TypeToken<List<com.buildit.master.models.SavedJobs>>() {
            }.getType();
            if(sSavedJobs.contains("master_id")) {
                savedJobses = getPostListFromJson(sSavedJobs, type);
            }
        }
    }

    public void setSavedJobs() {
        for (Job job : jobs) {
            for (com.buildit.master.models.SavedJobs savedJobse : savedJobses) {
                if (job.getJobId() == savedJobse.getJobId()) {
                    job.setSaved(true);
                }
            }
        }
    }

    public static <T> List<T> getPostListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

}
