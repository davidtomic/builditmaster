package com.buildit.master.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.buildit.master.R;
import com.buildit.master.adapters.MessageListAdapter;
import com.buildit.master.models.Messages;
import com.buildit.master.requests.Request;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomicdavid on 11/9/17.
 */

public class Message extends Fragment {

    RecyclerView mRecyclerView;

    Context context;

    String currentUserEmail;

    List<Messages> messages = new ArrayList<>();

    private MessageListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list, container, false);

        context = getActivity();

        currentUserEmail = getArguments().getString("currentUserEmail");

        mRecyclerView = view.findViewById(R.id.message_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        getMessages();

        adapter = new MessageListAdapter(messages, context);
        mRecyclerView.setAdapter(adapter);
        return view;
    }

    public void getMessages() {
        Request.GetMasterMessages getMasterMessages = new Request.GetMasterMessages();

        String jsonString = null;

        try {
            jsonString = getMasterMessages.execute(currentUserEmail).get();
            jsonString.trim();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Type listType = new TypeToken<List<Messages>>() {
        }.getType();

        if(jsonString.contains("message_id")) {
            messages = getPostListFromJson(jsonString, listType);
        }
    }

    public static <T> List<T> getPostListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }
}
