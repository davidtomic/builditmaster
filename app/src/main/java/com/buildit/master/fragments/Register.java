package com.buildit.master.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.buildit.master.R;
import com.buildit.master.requests.Request;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class Register extends Fragment {

    private Context context;

    private EditText username;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private Button registerButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        context = getActivity();
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        username = (EditText)view.findViewById(R.id.register_name);
        email = (EditText)view.findViewById(R.id.register_email);
        password = (EditText)view.findViewById(R.id.register_password);
        confirmPassword = (EditText)view.findViewById(R.id.register_confirm_password);
        registerButton = (Button)view.findViewById(R.id.register_button);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().length() > 5 && email.getText().toString().contains("@")) {
                    if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                        Request.RegisterRequest registerRequest = new Request.RegisterRequest();
                        try {
                            registerRequest.execute(username.getText().toString(), email.getText().toString(), password.getText().toString());
                            Toast.makeText(context, "User Registered successfully", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(context, "HttpRequestFailed", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(context, "Password does not match", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Email and password for security please.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
