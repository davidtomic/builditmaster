package com.buildit.master.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.buildit.master.R;
import com.buildit.master.activities.JobsActivity;
import com.buildit.master.requests.Request;

import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 10/25/17.
 */

public class Login extends Fragment {

    private Context context;

    private EditText email;
    private EditText password;

    private String loginEmail;
    private String loginPassword;

    private Button loginButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);
        context = getActivity();

        email = (EditText) view.findViewById(R.id.login_email);
        password = (EditText) view.findViewById(R.id.login_password);
        loginButton = (Button) view.findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginEmail = email.getText().toString();
                loginPassword = password.getText().toString();
                try {
                    Request.LoginRequests loginRequests = new Request.LoginRequests();
                    boolean isSuccessfull = loginRequests.execute(loginEmail, loginPassword).get();
                    if (isSuccessfull) {
                        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("user_email", loginEmail);
                        editor.putString("user_password", loginPassword);
                        editor.apply();
                        editor.commit();
                        Intent in = new Intent(context, JobsActivity.class);
                        startActivity(in);
                        getActivity().finish();
                    } else {
                        Toast.makeText(context, "Login failed.", Toast.LENGTH_LONG).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }


}
