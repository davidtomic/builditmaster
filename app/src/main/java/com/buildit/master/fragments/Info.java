package com.buildit.master.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.buildit.master.R;

/**
 * Created by tomicdavid on 11/10/17.
 */

public class Info extends Fragment {

    private Context context;

    private Button loginButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_login, container, false);
        context = getActivity();

        return view;

    }
}
