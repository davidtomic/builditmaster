package com.buildit.master.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.buildit.master.R;
import com.buildit.master.adapters.CustomExpandableListAdapter;
import com.buildit.master.adapters.ExpandableListData;
import com.buildit.master.requests.Request;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by tomicdavid on 11/3/17.
 */

public class AboutMaster extends Fragment {

    View aboutMaster;

    private Context context;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListData;
    HashMap<String, List<String>> expadanbleListDetails;

    View view;
    View view1;

    com.buildit.master.models.AboutMaster sAboutMaster;

    String sAbout;
    String currentUserEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_master_about, container, false);

        context = getActivity();
        view1 = view.findViewById(R.id.about_master);

        getAboutMaster();

        setExpandableList();

        return view;
    }

    public void setExpandableList(){
        expandableListView = view1.findViewById(R.id.master_type);

        expadanbleListDetails = ExpandableListData.getData();
        expandableListData = new ArrayList<>(expadanbleListDetails.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(context, expandableListData, expadanbleListDetails);

        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener(){

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(context,
                        expandableListData.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(context,
                        expandableListData.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        context,
                        expandableListData.get(groupPosition)
                                + " -> "
                                + expadanbleListDetails.get(expandableListData.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    public void getAboutMaster(){
        currentUserEmail = getArguments().getString("currentUserEmail");

        try {
            sAbout = new Request.GetAboutMaster().execute(currentUserEmail).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Type type = new TypeToken<com.buildit.master.models.AboutMaster>(){}.getType();

        sAboutMaster = getPostListFromJson(sAbout, type);
    }

    @Nullable
    public static com.buildit.master.models.AboutMaster getPostListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

}